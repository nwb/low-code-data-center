package com.yabushan.activiti.domain;

/**
 * @Author yabushan
 * @Date 2021/6/22 23:30
 * @Version 1.0
 */

import java.awt.*;

/**
 * 变量类
 **/
public final class WorkflowConstants {
    /**
     * 动态流程图颜色定义
     **/
    public static final Color COLOR_NORMAL = new Color(0, 205, 0);
    public static final Color COLOR_CURRENT = new Color(255, 0, 0);

    /**
     * 定义生成流程图时的边距(像素)
     **/
    public static final int PROCESS_PADDING = 5;

}
