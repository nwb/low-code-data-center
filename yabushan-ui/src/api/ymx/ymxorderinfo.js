import request from '@/utils/request'

// 查询订单列表
export function listYmxorderinfo(query) {
  return request({
    url: '/ymx/ymxorderinfo/list',
    method: 'get',
    params: query
  })
}

// 查询订单详细
export function getYmxorderinfo(orderId) {
  return request({
    url: '/ymx/ymxorderinfo/' + orderId,
    method: 'get'
  })
}

// 新增订单
export function addYmxorderinfo(data) {
  return request({
    url: '/ymx/ymxorderinfo',
    method: 'post',
    data: data
  })
}

// 修改订单
export function updateYmxorderinfo(data) {
  return request({
    url: '/ymx/ymxorderinfo',
    method: 'put',
    data: data
  })
}

// 删除订单
export function delYmxorderinfo(orderId) {
  return request({
    url: '/ymx/ymxorderinfo/' + orderId,
    method: 'delete'
  })
}

// 导出订单
export function exportYmxorderinfo(query) {
  return request({
    url: '/ymx/ymxorderinfo/export',
    method: 'get',
    params: query
  })
}