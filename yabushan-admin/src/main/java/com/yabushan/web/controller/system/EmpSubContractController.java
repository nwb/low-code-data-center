package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubContract;
import com.yabushan.system.service.IEmpSubContractService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工合同子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/contract")
public class EmpSubContractController extends BaseController
{
    @Autowired
    private IEmpSubContractService empSubContractService;

    /**
     * 查询员工合同子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:contract:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubContract empSubContract)
    {
        startPage();
        List<EmpSubContract> list = empSubContractService.selectEmpSubContractList(empSubContract);
        return getDataTable(list);
    }

    /**
     * 导出员工合同子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:contract:export')")
    @Log(title = "员工合同子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubContract empSubContract)
    {
        List<EmpSubContract> list = empSubContractService.selectEmpSubContractList(empSubContract);
        ExcelUtil<EmpSubContract> util = new ExcelUtil<EmpSubContract>(EmpSubContract.class);
        return util.exportExcel(list, "contract");
    }

    /**
     * 获取员工合同子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:contract:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubContractService.selectEmpSubContractById(recId));
    }

    /**
     * 新增员工合同子集
     */
    @PreAuthorize("@ss.hasPermi('system:contract:add')")
    @Log(title = "员工合同子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubContract empSubContract)
    {
        return toAjax(empSubContractService.insertEmpSubContract(empSubContract));
    }

    /**
     * 修改员工合同子集
     */
    @PreAuthorize("@ss.hasPermi('system:contract:edit')")
    @Log(title = "员工合同子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubContract empSubContract)
    {
        return toAjax(empSubContractService.updateEmpSubContract(empSubContract));
    }

    /**
     * 删除员工合同子集
     */
    @PreAuthorize("@ss.hasPermi('system:contract:remove')")
    @Log(title = "员工合同子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubContractService.deleteEmpSubContractByIds(recIds));
    }
}
