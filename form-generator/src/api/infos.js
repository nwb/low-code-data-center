import request from '@/utils/request'


// 查询自定义表单详细
export function getInfos(id) {
  return request({
    url: '/form/api/getOneInfo/' + id,
    method: 'get'
  })
}

// 新增自定义表单
export function addInfos(data) {
  return request({
    url: '/form/api/addinfos',
    method: 'post',
    data: data
  })
}
export function getUrlKey(name,url){
　　return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(url) || [, ""])[1].replace(/\+/g, '%20')) || null

}

// 插入数据
export function insertRow(data) {
  return request({
    url: '/form/api/insertRow',
    method: 'post',
    data: data
  })
}