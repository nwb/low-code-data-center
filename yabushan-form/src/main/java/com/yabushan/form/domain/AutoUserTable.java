package com.yabushan.form.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.yabushan.common.annotation.Excel;
import com.yabushan.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *  动态表信息对象 auto_user_table
 *
 * @author yabushan
 * @date 2021-08-06
 */
public class AutoUserTable extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 表ID */
    private String bId;

    /** 表中文名 */
    @Excel(name = "表中文名")
    private String bCnName;

    /** 表英文名 */
    @Excel(name = "表英文名")
    private String bEnName;

    /** 表数据行数 */
    @Excel(name = "表数据行数")
    private Long bRowNum;

    /** 所属租户 */
    @Excel(name = "所属租户")
    private Long uId;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updatedBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    public void setbId(String bId)
    {
        this.bId = bId;
    }

    public String getbId()
    {
        return bId;
    }
    public void setbCnName(String bCnName)
    {
        this.bCnName = bCnName;
    }

    public String getbCnName()
    {
        return bCnName;
    }
    public void setbEnName(String bEnName)
    {
        this.bEnName = bEnName;
    }

    public String getbEnName()
    {
        return bEnName;
    }
    public void setbRowNum(Long bRowNum)
    {
        this.bRowNum = bRowNum;
    }

    public Long getbRowNum()
    {
        return bRowNum;
    }
    public void setuId(Long uId)
    {
        this.uId = uId;
    }

    public Long getuId()
    {
        return uId;
    }
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime()
    {
        return updatedTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("bId", getbId())
            .append("bCnName", getbCnName())
            .append("bEnName", getbEnName())
            .append("bRowNum", getbRowNum())
            .append("uId", getuId())
            .append("createdBy", getCreatedBy())
            .append("createdTime", getCreatedTime())
            .append("updatedBy", getUpdatedBy())
            .append("updatedTime", getUpdatedTime())
            .toString();
    }
}
