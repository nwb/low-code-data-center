package com.yabushan.form.mapper;

import java.util.List;
import com.yabushan.form.domain.AutoTableFileds;

/**
 * 动态字段信息Mapper接口
 * 
 * @author yabushan
 * @date 2021-08-06
 */
public interface AutoTableFiledsMapper 
{
    /**
     * 查询动态字段信息
     * 
     * @param fId 动态字段信息ID
     * @return 动态字段信息
     */
    public AutoTableFileds selectAutoTableFiledsById(String fId);

    /**
     * 查询动态字段信息列表
     * 
     * @param autoTableFileds 动态字段信息
     * @return 动态字段信息集合
     */
    public List<AutoTableFileds> selectAutoTableFiledsList(AutoTableFileds autoTableFileds);

    /**
     * 新增动态字段信息
     * 
     * @param autoTableFileds 动态字段信息
     * @return 结果
     */
    public int insertAutoTableFileds(AutoTableFileds autoTableFileds);

    /**
     * 修改动态字段信息
     * 
     * @param autoTableFileds 动态字段信息
     * @return 结果
     */
    public int updateAutoTableFileds(AutoTableFileds autoTableFileds);

    /**
     * 删除动态字段信息
     * 
     * @param fId 动态字段信息ID
     * @return 结果
     */
    public int deleteAutoTableFiledsById(String fId);

    /**
     * 批量删除动态字段信息
     * 
     * @param fIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteAutoTableFiledsByIds(String[] fIds);
}
